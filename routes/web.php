<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/storageLink', 'HomeController@storageLink');

Auth::routes(['register' => false]);

// PUBLIC
Route::get('/', 'HomeController@index');
Route::get('/formation', function(){ return view('site.formation'); });
Route::get('/presentation', function(){ return view('site.presentation'); });
Route::get('/poursuite', function (){ return view('site.poursuite'); });
Route::get('/welcome', function(){ return view('site.welcome'); });
Route::get('/crew', function(){ return view('site.crew'); });
Route::get('/partenaires', function(){ return view('site.partenaires'); });
Route::get('/iut-france', function(){ return view('site.iut-france'); });
Route::get('/debouches', function() { return view('site.debouches'); });
Route::get('/dut-info', function() { return view('site.dut-info'); });
Route::get('/temoignages', 'HomeController@listTestimonials');
Route::get('/temoignages/{id}', 'HomeController@showTestimonial');
Route::get('/actualites', 'HomeController@listArticles');
Route::get('/articles/{id}', 'HomeController@showArticle');
Route::get('/licences-pro', 'HomeController@listLicencesPro');
Route::get('/licences-pro-specialisation', 'HomeController@listLicencesProSpecial');
Route::get('/annonces', 'HomeController@listAnnounces');
Route::get('/annonces/{id}', 'HomeController@showAnnounce');
Route::get('/nouvelle-annonce', 'HomeController@createAnnounce');
Route::post('/nouvelle-annonce', 'AnnounceController@store');

// ADMIN
Route::middleware('is_admin')->prefix('admin')->group(function(){
    Route::get('/', 'AdminController@index');
    Route::resource('/temoignages', 'TemoignageController');
    Route::resource('/articles','ArticleController');
    Route::resource('/stages', 'AnnounceController');
    Route::resource('/users', 'UserController');
    Route::resource('/announces', 'AnnounceController');

    // AJAX ROUTES
    Route::get('/temoignages/{id}/toggleVisibility','TemoignageController@toggleVisibility')->name('temoignages.toggleVisibility');
    Route::get('/articles/{id}/toggleVisibility','ArticleController@toggleVisibility')->name('articles.toggleVisibility');
    Route::get('/announces/{id}/toggleVisibility','AnnounceController@toggleVisibility')->name('announces.toggleVisibility');
});

// LOCALE
Route::get('locale/{locale}',function($locale){
    Session::put('locale',$locale);
    return redirect()->back();
});
