<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at' , 'desc')->simplePaginate(15);
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        return view('admin.articles.create');

    }

    public function store(Request $request){


        $values = request()->validate([
            'title' => 'required|max:255' ,
            'author' => 'nullable|string|max:100',
            'description' => 'required'
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = $request->image->store('image', 'public');
            $values['photo'] = $path;
        }
        // else {
        //     dd($request->file('photo'));
        // }

        Article::create($values);

        return redirect('/admin/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('admin.articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('admin.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $values = request()->validate([
            'title' => 'required|max:255' ,
            'author' => 'nullable|string|max:100',
            'description' => 'required'
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = $request->image->store('image', 'public');
            $values['photo'] = $path;
        }
        // else {
        //     dd($request->file('image'));
        // }

        $article->update($values);

        return redirect('/admin/articles')->with('status', 'modif');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return redirect('/admin/articles');
    }

    public function toggleVisibility(int $id)
    {
        $article = Article::findOrFail($id);
        $article->visible = ! $article->visible ;
        $article->update();


        return redirect('/admin/articles');
    }
}
