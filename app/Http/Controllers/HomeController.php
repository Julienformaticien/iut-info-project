<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use App\Temoignage;
use App\Announce;
use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = Article::where('visible', true)->orderBy('created_at', 'desc')->take(7)->get();
        $temoignages = Temoignage::where('visible', true)->orderBy('created_at', 'desc')->take(8)->get();
        return view('site.index', ['articles' => $articles, 'temoignages' => $temoignages]);
    }

    public function listTestimonials()
    {
        return view(
            'site.temoignages',
            ['temoignages' => DB::table('temoignages')->orderBy('created_at', 'desc')->where('visible',true)->simplePaginate(9)]
        );
    }

    public function listArticles()
    {
        return view(
            'site.actualites',
            ['actualites' => DB::table('articles')->where('visible',true)->orderBy('created_at', 'desc')->simplePaginate(9)]
        );
    }

    public function showArticle(Request $request, $id)
    {
        if(DB::table('articles')->find($id)->visible == 'false' ){
            abort(404);
        }
        return view(
            'site.actualite',
            ['actu' => DB::table('articles')->find($id)]
        );
    }
    public function showTestimonial(Request $request, $id)
    {
        if(DB::table('temoignages')->find($id)->visible == 'false' ){
            abort(404);
        }
        return view(
            'site.temoignage',
            ['temoignage' => DB::table('temoignages')->find($id)]
        );
    }

    public function listLicencesPro()
    {
        return view('site.licences-pro');
    }

    public function listLicencesProSpecial()
    {
        $licencesPro = DB::table('licence_pros')->orderby('city')->simplePaginate(10);

        return view(
            'site.licences-pro-special', compact('licencesPro')
        );
    }

    public function listLicencesProDoubleComp()
    {
        return view('site.licences-pro');
    }

    public function listAnnounces()
    {
        $announces =  Announce::where(['validated'=> true, 'visible'=>true])->orderBy('created_at', 'desc')->simplePaginate(10);
        return view('site.announces', compact('announces'));
    }

    public function createAnnounce()
    {
        return view('site.create-announce');
    }
    public function showAnnounce(Request $request, $id)
    {
        if(DB::table('announces')->find($id)->visible == 'false' ){
            abort(404);
        }
        return view(
            'site.announce',
            ['announce' => DB::table('announces')->find($id)]
        );
    }

    public function storageLink(){
        Artisan::call("storage:link");
        return redirect('/')->with('status', "Storage Link activé avec succès !");
    }
}
