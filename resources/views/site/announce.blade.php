@extends ('layouts.site')

@section('title', 'Annonce')

@section('content')
    <div class="header-connexe">
        <div class="title-accueil"><h1>{{ $announce->company_name}}</h1></div>
    </div>


    <div class="contentContainer">
        <div class="contentSubcontainer">
            <p class="typicalTitle wrapper">{{ $announce->title}}</p>
            <p class="typicalSubtitle wrapper">Entreprise</p>
            <p class="info-stage wrapper"><strong>Nom : </strong>{{ $announce->company_name}}</p>
            @if ( !is_null($announce->company_country))
                <p class="info-stage wrapper"><strong>Pays : </strong>{{ $announce->company_country}}</p>
            @endif
            @if ( !is_null($announce->company_city))
                <p class="info-stage wrapper"><strong>Ville : </strong>{{ $announce->company_city}}</p>
            @endif

            @if ( !is_null($announce->company_address))
                <p class="info-stage wrapper"><strong>Adresse : </strong>{{ $announce->company_address}}</p>
            @endif

            @if ( !is_null($announce->company_zipcode))
            <p class="info-stage wrapper"><strong>Cope postal : </strong><span class="number">{{ $announce->company_zipcode}}</span></p>
            @endif


            @if (!is_null($announce->contact_firstname) || !is_null($announce->contact_lastname) || !is_null($announce->contact_phone) || !is_null($announce->contact_email))
                <p class="typicalSubtitle wrapper">Contact</p>
                @if ( !is_null($announce->contact_firstname) || !is_null($announce->contact_lastname))
                    <p class="info-stage wrapper"><strong>Nom : </strong>{{ $announce->contact_firstname}}  {{ $announce->contact_lastname}}</p>
                @endif
                @if ( !is_null($announce->contact_phone))
                    <p class="info-stage wrapper"><strong>Téléphone : </strong><span class="number">{{ $announce->contact_phone}}</span></p>
                @endif

                @if ( !is_null($announce->contact_email))
                    <p class="info-stage wrapper"><strong>Email : </strong>{{ $announce->contact_email}}</p>
                @endif
            @endif




            <p class="typicalSubtitle wrapper">Description</p>
            <p class="info-stage wrapper"><strong>Mise en ligne de l'offre : </strong><span class="number">{{ \Carbon\Carbon::parse($announce->created_at)->format('d/m/Y H:i')}}</span></p>
            <p class="info-stage wrapper"><strong>Date de début : </strong><span class="number">{{ $announce->date_beginning}}</span></p>
            <p class="info-stage wrapper"><strong>Durée minimum : </strong>{{ $announce->minimum_duration}}</p>
            <p class="description-stage wrapper">{!! nl2br(e( $announce->description )) !!}</p>
        </div>

    </div>


@endsection
