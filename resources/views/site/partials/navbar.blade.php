<div class="navbar-toggle" onclick="navbarToggle();">
    <i class="fas fa-bars"></i>
</div>
<aside class="navbar">
    <a href="{{ url('/') }}" class="logo"><img src="{{ asset('img/logoW.png') }}"></a>
    <nav>
        <ul>
            <li id="home">
                <a href="{{ url('/') }}">
                    <i class="fa fa-home"></i>
                    @lang('navbar.home')
                </a>
            </li>
            <li id="iut">
                <span onclick="navbarDropdown(this);">
                    <i class="far fa-lightbulb"></i>
                    @lang('navbar.about')
                    <i class="fas fa-chevron-down"></i>
                </span>
                <ul>
                    <li id="presentation">
                        <a href="{{ url('/presentation') }}">@lang('navbar.pres')</a>
                    </li>
                    <li id="formation">
                        <a href="{{ url('/formation') }}">@lang('navbar.formation')</a>
                    </li>
                    <li id="map">
                        <a href="{{ url('/iut-france') }}">@lang('navbar.IUTInFr')</a>
                    </li>
                </ul>
            </li>
            <li id="formations">
                <span onclick="navbarDropdown(this);">
                    <i class="fa fa-graduation-cap"></i>
                    @lang('navbar.formations')
                    <i class="fas fa-chevron-down"></i>
                </span>
                <ul>
                    <li id="dut">
                        <a href="{{ url('/dut-info') }}">@lang('navbar.dut')</a>
                    </li>
                    <li id="lp">
                        <a href="{{ url('/licences-pro') }}">@lang('navbar.lp')</a>
                    </li>
                </ul>
            </li>
            <li id="apres">
                <span onclick="navbarDropdown(this);">
                    <i class="fas fa-graduation-cap"></i>
                    @lang('navbar.after')
                    <i class="fas fa-chevron-down"></i>
                </span>
                <ul>
                    <li id="debouche">
                        <a href="{{ url('/debouches') }}">@lang('navbar.deb')</a>
                    </li>
                    <li id="poursuite">
                        <a href="{{ url('/poursuite') }}">@lang('navbar.studies')</a>
                    </li>
                </ul>
            </li>
            <li id="entreprises">
                <span onclick="navbarDropdown(this);">
                    <i class="far fa-handshake"></i>
                    @lang('navbar.companies')
                    <i class="fas fa-chevron-down"></i>
                </span>
                <ul>
                    <li id="partenaires">
                        <a href="{{ url('/partenaires') }}">@lang('navbar.part')</a>
                    </li>
                    <li id="annonces">
                        <a href="{{ url('/annonces') }}">@lang('navbar.offers')</a>
                    </li>

                </ul>
            </li>
            <li id="actualites">
                <a href="{{ url('/actualites') }}">
                    <i class="far fa-newspaper"></i>
                    @lang('navbar.news')
                </a>
            </li>
            <li id="temoignages">
                <a href="{{ url('/temoignages') }}">
                    <i class="fas fa-comment-alt"></i>
                    @lang('navbar.tem')
                </a>
            </li>
        </ul>
    </nav>
    <div class="langs">
        <a href="{{ url('/locale/fr') }}" class="{{ Session('locale') == 'fr' ? 'active' : '' }}">FR</a>
        <a href="{{ url('/locale/en') }}" class="{{ Session('locale') == 'en' ? 'active' : '' }}">EN</a>
    </div>
</aside>
