<footer>
	<div class="footer-title">
		<h3>@lang('footer.contactUs')</h3>
	</div>
	<div class="link-footer">
		<ul>
			<li><a href="mailto:contact@iut-informatique.fr">@lang('footer.contact')</a></li>
			<li><a href="/iut-france">@lang('navbar.IUTInFr')</a></li>
			<li><a href="mailto:webmaster@iut-informatique.fr">@lang('footer.report')</a></li>
			<li><a href="/crew">@lang('footer.crew')</a></li>
		</ul>
	</div>
	<div class="made-footer">
		<h6>&copy; I.U.T. Informatique France, made by Crew Core 2019</h6>
	</div>
</footer>