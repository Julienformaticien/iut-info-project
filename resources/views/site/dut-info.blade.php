@extends ('layouts.site')

@section('title', 'DUT Informatique')

@section('content')
<div class="header-connexe">
    <div class="title-accueil">
        <h1>@lang('dutinfo.header')</h1>
    </div>
</div>
<div class="contentContainer">

    <div class="contentSubcontainer">
        <h1 class="typicalTitle wrapper">@lang('dutinfo.intro')</h1>
        <p class="typicalText wrapper">@lang('dutinfo.introtext')</p>
        <div class="typical-button-holder">
            <a href="/formation">@lang('dutinfo.discover')</a>
        </div>
    </div>
    <div class="contentSubcontainer">
        <h1 class="typicalSubtitle wrapper">@lang('dutinfo.jobexample')</h1>
        <ul class="typicalText wrapperList">
            <li>- @lang('dutinfo.job1')</li>
            <li>- @lang('dutinfo.job2')</li>
            <li>- @lang('dutinfo.job3')</li>
            <li>- @lang('dutinfo.job4')</li>
            <li>- @lang('dutinfo.job5')</li>
            <li>- @lang('dutinfo.job6')</li>
            <li>- @lang('dutinfo.jobothers')</li>
        </ul>
    </div>
    <div class="contentSubcontainer">
        <h1 class="typicalSubtitle wrapper">@lang('dutinfo.access')</h1>
        <p class="typicalText wrapper">@lang('dutinfo.accesstext')</p>
        <p class="typicalText wrapper">@lang('dutinfo.accesstext2')</p>
    </div>
    
</div>
@endsection

@section('javascript')
    <script>
        $(function(){
                $("#formations").addClass("open");
                $("#dut").addClass("here");
            });
    </script>
@endsection
