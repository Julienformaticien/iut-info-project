@extends ('layouts.site')

@section('title', 'Accueil')

@section('content')
<div class="header-main">

	<div class="arrow bounce">
		<p><i class="fa fa-arrow-down fa-2x theArrow" href="#"></i></p>
	</div>

	<div class="position-header">
		<div class="left-position-header">
			<h1 data-aos="fade-zoom-in" data-aos-delay="300">@lang('home.title1')</h1>
			<h2 data-aos="fade-zoom-in" data-aos-delay="500">@lang('home.title2')</h2>
		</div>
		<div class="right-position-header" data-aos="fade-left" data-aos-delay="600">
			<img src="{{ asset('img/web.png') }}" />
		</div>

		<div class="button-header">
			<div class="right-button-header">
				<img src="{{ asset('img/points.png') }}">
			</div>
			<div class="left-button-header">
				<a href="/formation">@lang('home.decouvrir')</a>
			</div>

		</div>
	</div>

</div>

<div class=" hero-container accueil-hero">
	<div data-aos="fade-up" data-aos-delay="300" class="hero-text-holder">
		<div class="screen hero-text">
			<div class="container-button-windows">
				<div class="button-windows red"></div>
				<div class="button-windows yellow"></div>
				<div class="button-windows green"></div>
			</div>
			<div class="title-screen">
				<h2>@lang('home.why')</h2>
			</div>
			<div id="typed-strings">
				<p class="cursive"><span class="color-purple">$ </span> @lang('home.typed1') </p>
				<p><span class="color-purple">$ </span>@lang('home.typed2')</p>
			</div>
			<span id="typed"></span>
		</div>
	</div>
	<canvas id="canvas-presentation" class="canvas-image-blending"></canvas>
</div>

<div class="carousel-container">
	<p class="carousel-header wrapper"><span class="carousel-header-purple">@lang('home.news')</span> @lang('home.and') <span class="carousel-header-green">@lang('home.actu') </span>
		<span class="carousel-subheader">\ @lang('home.frenchIUTs')</span>
	</p>
	{{--
	<div class="fades" style="position: relative;">
		<div class="fade-left"></div>
		<div class="fade-right"></div>
	</div> --}}
	<div style="position: relative;">
		<img src="{{ asset('svg/monitor.svg') }}" alt="" class="screen-bg">
	</div>


	<div class="carousel hidden" id="carousel-news">

		@foreach ($articles as $article)
		<div class="carousel-element news">
			<div class="news-content">

				<div class="news-button-holder">
					<a href="/articles/{{ $article->id }}" class="news-button">
						<p class="news-button-text">@lang('home.article')</p>
						<img src="{{ asset('img/arrow_white.png')}}" alt="" class="news-button-arrow">
					</a>
				</div>
				@if ( is_null($article->photo) )
					<div style="background-image: url({{ asset('img/article_placeholder.jpg') }});" class="news-img"></div>
				@else
					<div style="background-image: url(storage/{{ $article->photo }});" class="news-img"></div>
				@endif
				
				<a href="/articles/{{ $article->id }}" style="">
					<p class="news-title">{{ str_limit($article->title, $limit = 50, $end = '...') }}</p>
				</a>
				<p class="news-date"><i class="far fa-clock"></i> {{ \Carbon\Carbon::parse($article->created_at)->format('d/m/Y H:i')}}</p>
				<p class="news-text">{{ str_limit($article->description, $limit = 110, $end = '...') }}</p>
			</div>
		</div>
		@endforeach

		<div class="carousel-element news-empty">
			<a href="/actualites">
				<div class="news-content empty">
					<p class="news-empty-text">@lang('home.moreArticles')</p>
					<img src="{{ asset('img/right-arrow-purple.png') }}" alt="">
				</div>
			</a>
		</div>


	</div>
</div>

<div class="carousel-container">
	<p class="carousel-header wrapper"><span class="carousel-header-purple">@lang('home.test')</span> <span class="carousel-subheader">\ @lang('home.avForm')</span></p>
	{{--
	<div class="fades" style="position: relative;">
		<div class="fade-left"></div>
		<div class="fade-right"></div>
	</div> --}}
	<div style="position: relative;">
		<img src="{{ asset('svg/monitor.svg') }}" alt="" class="screen-bg">
	</div>

	<div class="carousel hidden" id="carousel-testimonial">
		@foreach ($temoignages as $temoignage)
		{{-- <a style="text-decoration: none; cursor: auto;" href="#"> --}}
			<div class="carousel-element">
				<div class="testimonial-content gradient">
                    <a href="/temoignages/{{ $temoignage->id}}">
                        @if( !is_null($temoignage->image) )
                            <div style="background-image: url(storage/{{ $temoignage->image }});" class="testimonial-img"></div>
                        @else
                            <div style="background-image: url({{ asset('img/user_placeholder.png') }});" class="testimonial-img"></div>
                        @endif
                        <p class="testimonial-name">{{ $temoignage->author }}</p>
                        <p class="testimonial-job">{{ $temoignage->author_info }}</p>
                        <div class="quote-holder">
                            <img src="{{ asset('img/quote.png')}}" alt="" class="quote"> {{-- <img src="{{ asset('img/quote.png')}}" alt="" class="quote-bottom">						--}}
                        </div>
                        <p class="testimonial-text">{{ str_limit($temoignage->title, $limit = 50, $end = '...') }}</p>
                        <p class="testimonial-description" style="height: 100px; opacity: 1;">{{ str_limit($temoignage->description, $limit = 150, $end = '...') }}
                        </p>
                        {{--
                        <a class="testimonial-button" href="/temoignages/{{ $temoignage->id }}">
                            <p>@lang('home.readMore')</p>
                        </a> --}}
                    </a>

				</div>
			</div>
		{{-- </a> --}}

		@endforeach




		<div class="carousel-element">
			<div class="testimonial-content gradient-purple">
				<a href="/temoignages" class="nostyle-testimonial">
					<div class="testimonial-content empty">
						<p class="testimonial-empty-text">@lang('home.moreTem')</p>
						<img src="{{ asset('img/right-arrow-white.png') }}" alt="">
					</div>
				</a>
			</div>
		</div>


	</div>

</div>
@endsection

@section('javascript')
	<script src="{{ mix('dist/js/home.js') }}"></script>

	
		@if(session()->has('status'))
			<script>
				Swal.fire("Succès", '{{ session('status') }}', 'success');
			</script>
        @endif
    

    <script>
        $(function(){
                $("#home").addClass("open");
            });
    </script>
@endsection
