@extends ('layouts.site')

@section('title', 'Actualités')

@section('content')
    <div class="header-connexe">
        <div class="title-accueil"><h1>@lang('news.home')</h1></div>
    </div>
    <div class="newsCardContainer" style="min-height: 40vh;">
        {{-- <strong>Page en travaux ! </strong> --}}

       @foreach($actualites as $article)
            <div class="news-element news">
                <div class="news-content-fix">
                    <div class="news-button-holder">
                        <a href="/articles/{{ $article->id }}" class="news-button">
                            <p class="news-button-text">@lang('home.article')</p>
                            <img src="{{ asset('img/arrow_white.png')}}" alt="" class="news-button-arrow">
                        </a>
                    </div>
                    @if ( is_null($article->photo) )
                        <div style="background-image: url({{ asset('img/article_placeholder.jpg') }});" class="news-img"></div>
                    @else
                        <div style="background-image: url(storage/{{ $article->photo }});" class="news-img"></div>
                    @endif
                    <a href="/articles/{{ $article->id }}">
                        <p class="news-title">{{ $article->title }}</p>
                    </a>
                    <p class="news-date"><i class="far fa-clock"></i> {{ \Carbon\Carbon::parse($article->created_at)->format('d/m/Y H:i')}}</p>
                    <p class="news-text">{{ str_limit($article->description, $limit = 110, $end = '...') }}</p>
                </div>
            </div>
        @endforeach

    </div>
    <div class="contentContainer">
        {{ $actualites->links('vendor.pagination.default-iut', ['elements' => $actualites]) }}
    </div>
@endsection

@section('javascript')
    <script>
        $(function(){
                $("#actualites").addClass("open");
            });
    </script>
@endsection
