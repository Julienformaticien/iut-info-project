@extends ('layouts.site')

@section('title', 'IUT en France')

@section('content')

    <div class="header-connexe">
        <div class="title-accueil">
            <h1>Carte des I.U.T. en France</h1>
        </div>
    </div>

    <div class="container mapContainer" data-aos="fade-zoom-in">
        <div class="map">
            <p class="default-unload-map">Impossible de charger la carte</p>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js" charset="utf-8"></script>
    <script src="{{ asset('js/jquery.mapael.js') }}" charset="utf-8"></script>
    <script src="{{ asset('js/france_departments.js') }}" charset="utf-8"></script>
    <script src="{{ asset('js/map.js') }}"></script>

    <script>
        $(function(){
                $("#iut").addClass("open");
				$("#map").addClass("here");
            });
    </script>
@endsection
