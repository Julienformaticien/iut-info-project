@extends('layouts.admin')

@section('title', trans("news.create"))

@section('content')
    <div class="admin-create-container">

        <div class="admin-create-content">

            <h1 class="title">Créer un nouvel article </h1>
            <form method="POST" action="/admin/articles" enctype='multipart/form-data'>
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="field">
                    <label for="" class="label">Titre</label>
                    <input class="input {{ $errors->has('title') ? 'is-danger' : ''}}"
                           type="text" name="title"
                           placeholder="Titre de l'article"
                           required
                           value="{{ old('title') }}"
                           maxlength="100">
                </div>

                <div class="field">
                    <label for="">Image de l'article</label>
                    <div class="file has-name">
                        <label class="file-label">
                            <input class="file-input" type="file" name="image" id="photo">
                            <span class="file-cta">
                                <span class="file-icon">
                                <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                Choisissez un fichier…
                                </span>
                            </span>
                            <span class="file-name" id="filename">
                            </span>
                        </label>
                    </div>
                </div>

                <div class="field">
                    <label for="" class="label">Contenu de l'article</label>
                    <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}"
                              name="description"
                              placeholder="Article" required
                              value="{{ old('description') }}">{{ old('description') }}</textarea>
                </div>

                <div class="field">
                    <label class="label" for="">Auteur</label>
                    <input class="input {{ $errors->has('author') ? 'is-danger' : ''}}"
                           type="text" name="author"
                           placeholder="Auteur de l'article"
                           value="{{ old('author') }}">
                </div>

                <div>
                    <button class="button is-primary" type="submit">Créer l'article</button>
                </div>
            </form>
            @include('admin.partials.errors')
        </div>
    </div>
@endsection
