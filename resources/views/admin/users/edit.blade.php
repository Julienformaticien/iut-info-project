@extends('layouts.admin')

@section('title', trans("users.edit"))

@section('content')
    <div class="admin-create-container">
        <div class="admin-create-content">
            <h1 class="title">Modification d'un user</h1>

            <form method="POST" action="/admin/users/{{ $user->id }}">

                {{ csrf_field() }}

                {{ method_field('PATCH') }}

                <div class="field">
                    <label class="label" for="">Nom</label>
                    <input class="input" type="text" name="name" value="{{ $user->name }}">
                </div>

                <div class="field">
                    <label class="label" for="">Email</label>
                    <input class="input" type="email" name="email" value="{{ $user->email }}">
                </div>

                <div class="field">
                    <label class="label">Type de compte</label>
                    <div class="control">
                        <label class="radio" for="type-admin">
                            <input value="admin" type="radio" id="type-admin" name="type" required {{ $user->type == 'admin' ? 'checked' : ''  }}>
                            Admin
                        </label>
                        <label class="radio" for="type-default">
                            <input value="default" type="radio" id="type-default" name="type" required {{ $user->type == 'default' ? 'checked' : ''  }}>
                            Modérateur
                        </label>
                    </div>
                </div>

                <input class="button is-primary" type="submit" name="ok" value="Valider les modifications">
            </form>

            @if( auth()->user()->isAdmin() &&  auth()->user()->id !== $user->id )
                <form method="POST" action="/admin/users/{{ $user->id }}">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}

                    <div>
                        <div class="suppr-article">
                            <br>
                            <button class="button is-danger" type="submit">Supprimer l'utilisateur</button>
                        </div>
                    </div>
                </form>
            @endif

            @include('admin.partials.errors')
        </div>
    </div>
@endsection
