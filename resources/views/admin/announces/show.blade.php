@extends('layouts.admin')

@section('title', $stage->company_name )

@section('content')

<div class="header-main-temoignages">

    <div class="layout-article stage-layout"> 

        <p><strong>{{ $stage->company_name }}</strong></p>

        @if( !is_null($stage->company_address) )
            <p><i class="fas fa-map-marker-alt"></i> {{ $stage->company_address }}</p>
        @endif

        @if( !is_null($stage->phone) )
            <p><i class="fas fa-phone"></i> Téléphone : <a href="tel:{{ $stage->email }}">{{ $stage->phone }}</a></p>
        @endif

        @if( !is_null($stage->email) )
            <p><i class="fas fa-envelope"></i> Email : <a href="mailto:{{ $stage->email }}">{{ $stage->email }}</a></p>
        @endif

        @if( !is_null($stage->supervisor) )
            <p><i class="fas fa-user"></i> Maître de stage : {{ $stage->supervisor }}</p>
        @endif

        <h2>{{ $stage->title }}</h2>
        <p>{{ $stage->description }}</p>

        <div class="button-feed-container stage-button">
            <a href="/stages">
                <button>Tous les stages</button>
             </a>
        </div>
    </div>


</div>

@endsection
