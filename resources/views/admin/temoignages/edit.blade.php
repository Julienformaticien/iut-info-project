@extends('layouts.admin')

@section('title', trans("testimonial.edit"))

@section('content')
    <div class="admin-create-container">
        <div class="admin-create-content">

            <h1 class="title">Modification d'un témoignage</h1>

            <form method="POST" action="/admin/temoignages/{{ $temoignage->id }}" enctype='multipart/form-data'>
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <div class="field">
                    <label class="label">Nom de l'auteur du témoignage</label>
                    <input class="input" type="text" name="author" required value="{{ $temoignage->author }}" maxlength="100">
                </div>
                <div class="field">
                    <label class="label">Informations sur l'auteur</label>
                    <input class="input" type="text" name="author_info" required value="{{ $temoignage->author_info }}"
                           maxlength="255">
                </div>
                <div class="field">
                    <label for="">Photo de l'auteur</label>
                    <div class="file has-name">
                        <label class="file-label">
                            <input class="file-input" type="file" value="" name="image" id="author-photo">
                            <span class="file-cta">
                              <span class="file-icon">
                                <i class="fas fa-upload"></i>
                              </span>
                              <span class="file-label">
                                Choisissez un fichier…
                              </span>
                            </span>
                            <span class="file-name" id="filename">
                            </span>
                        </label>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Titre</label>
                    <input class="input" type="text" name="title" required value="{{ $temoignage->title }}" maxlength="100">
                </div>
                <div class="field">
                    <label class="label">Description</label>
                    <textarea class="textarea" type="text" name="description"
                              value="{{ $temoignage->description }}">{{ $temoignage->description }}</textarea>
                </div>
                <div class="field">
                    <input class="button is-primary" type="submit" name="ok" value="Valider les modifications">
                </div>
            </form>


            @if( auth()->user()->isAdmin() )
                <form method="POST" action="/admin/temoignages/{{ $temoignage->id }}">

                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}

                    <div>
                        <div class="suppr-temoignage">
                            <br>
                            <button class="button is-danger" type="submit">Supprimer le témoignage</button>
                        </div>
                    </div>

                </form>
            @endif


            @include('admin.partials.errors')

        </div>
    </div>
@endsection
