<nav class="navbar has-background-light" role="navigation" aria-label="main navigation">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="/admin">
                <img src="https://image.flaticon.com/icons/svg/25/25694.svg" alt="Home">
            </a>
        </div>

        <a class="navbar-item" href="/admin/articles">
            Articles
        </a>
        <a class="navbar-item" href="/admin/temoignages">
            Témoignages
        </a>
        <a class="navbar-item" href="/admin/announces">
            Annonces
        </a>

        {{-- TODO : a mettre si on a le temps
        <a class="navbar-item" href="/admin/iuts">
            IUT's
        </a>
        <a class="navbar-item" href="/admin/licences-pro">
            Licences pro
        </a>
        --}}

        <a class="navbar-item" target="blank" href="/admin/translations">
            Traductions
        </a>
        <a class="navbar-item" href="/admin/users">
            Utilisateurs
        </a>

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                    <div class="" aria-labelledby="navbarDropdown">
                        <a class="button is-primary"
                           href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
