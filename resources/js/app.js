require('./bootstrap');

// EASTER EGG
$(function () {
    var k = [66, 79, 85, 78, 67, 69];
    var n = 0;
    var bounce = false;

    $(document).keydown(function (e) {
        if (e.keyCode === k[n++]) {
            if (n === k.length) {
                Swal.fire({
                    type: 'success',
                    title: 'Bounce mode',
                    text: 'Click on any text to make it bounce !'
                });
                bounce = true;

                n = 0;
                return false;
            }
        }
        else {
            n = 0;
        }
    });

    $('p, h1, h2, h3, h4, h5, h6, li, img').click(function () {
        if (!$(this).hasClass("animation-target") & bounce == true ? $(this).addClass("animation-target") : $(this).removeClass("animation-target"));
    });
});


window.navbarDropdown = (el) => {
    $(".navbar > nav > ul > li.open").not($(el).parent()).removeClass("open");
    $(el).parent().toggleClass("open");
}

window.navbarToggle = () => {
    $(".navbar-toggle i").toggleClass("fa-bars");
    $(".navbar-toggle i").toggleClass("fa-times");
    $(".navbar").toggleClass("open");
}