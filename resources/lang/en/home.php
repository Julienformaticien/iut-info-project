<?php
return[
	'title1' => 'WELCOME',

	'title2' => 'to the french IUT',

	'decouvrir'=> 'Discover the formation',

	'typed1' => "The IT departments of the university technology institutes offer multidisciplinary and versatile training in the computer science professions.",

	'typed2'=> "They train students in programming, algorithmics, networks, systems, etc …",

	'why' => 'Why choose an I.U.T. ?',

	'article' => 'Read more',

	'moreArticles' => 'See more news',

	'moreTem' => 'Discover the testimonies',

	'readMore' => 'See more testimonies',

	'frenchIUTs'=> 'IUT in France',

	'avForm' => 'and opinions on training',

	'test' => 'Testimonies',

	'actu' => '',

	'and'=> '',

	'news' => 'News',

];
