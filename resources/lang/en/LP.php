<?php

return[

    'header' => "Professional licenses",
    
    'intro' => "Introduction",

    'introText' => "Given the multiplicity of IT professional licenses, the network of 46 IUT IT departments, with its experience over 40 years, decided to put forward their professional licenses. They have in common what has forged the reputation of computer DUTs:",

    'force1' => "technological education provided by professionals, teachers and researchers in the IT field",

    'force2' => "practical teaching with a lot of practical work and a professional project",

    'force3' => "high-performance hardware and software to meet the needs of the sector",

    'force4' => "constant supervision: regular monitoring and control, small group and team work",

    'force5' => "the permanent concern to be in adequacy with the economic fabric: alternation training, pre-employment internships",

    'force6' => "a selection of students at the entrance based on their motivation",

    'force7' => "practical experience in business with a long internship (4 to 5 months)",

    'LPSpecialHeader' => "Professional Licenses of Specialization in Computer Science",

    'LPSpecialText' => "They consist in specializing IT-based baccalaureate + 2 in a field or advanced technology (distributed or distributed information systems, system, network and DB administration, internet / intranet development, embedded computing, digital imaging and / or sound. ..). They are to be regarded as a third year of IUT and bring a high level of technicality. According to the diploma of access to bac + 2, students will have had more than 2100 hours of training, 2 or 3 projects tutored and 2 internships in companies.",

    'LPDiscover' => "Discover Professional Licenses",

    'LPDoubleCompHeader' => "Dual Competence Professional Licenses",

    'LPDoubleCompText' => "These licenses aim to bring a complementary skill to computer science or computer science. In the first case, teaching concerns both computer science and another discipline. In the latter case, they lay the foundation for computer analysis and programming.",


    'LPspecHeader' => "Professional Licenses of Specialization in Computer Science",

    'GLSI' => "Software Engineering, Information Systems, Object Technology and Distributed Computing",

    'MN' => "Web professions",

    'SR' => "Network Systems",

    'ISN' => "Image and / or digital sound",

    'IE' => "Embedded Computing",


];