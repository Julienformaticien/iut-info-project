<?php

return[

	'home' => 'Intership list',

	'create' => 'New internship',

    'edit' => "Edit internship",

    'search' => "Looking for an <span class=\"stage\">intern</span> or an <span class=\"apprentissage\">apprenticeship student</span> ?",

    'click' => "Click here"

];
