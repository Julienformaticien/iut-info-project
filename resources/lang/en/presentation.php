<?php

return[

	'header' => "Presentation of the IT University Institute of Technology",

    'title' => "IUT is",

    'nbIut' => "IUT in France",

    'nbAns' => "Year of being",

    '3forces' => "The strength and success of the IUT in 3 points:",

    '1' => "1",
    '1detail' => "Training that covers many sectors",

    '2' => "2",
    '2detail' => "A collaboration with professionals",

    '3' => "3",
    '3detail' => "Research professors",

    '6' => "6",
    'mois' => "months",

    'tpMoy' => "This is the average time it takes for a student to find work after the graduation",

    'VAE' => "VAE",

    'VAEdetails' => "Different teaching methods are set up, whiches are suitable for a larger number of students and professionals wishing to resume their studies or to do a \"validation of acquired experience\". (VAE).",

    'etranger' => "IUTs, in partnership with foreign universities, offer study abroad as well as internships.",

    'depInfo' => "IT departements",

    'depInfoDetails1' => "The teaching of computer science at the IUT makes a large part of the professional dimension while retaining fundamental components; the student acquires know-how but also knowledge, basic concepts and working methods that he will be able to use in a professional situation.",

    'depInfoDetails2' => "The DUT, university and professional diploma (120 European credits): The holders of the DUT in computer science are advanced technicians, able to participate in the design, implementation and implementation of computer systems. However, they can if they wish to continue their studies at the end of this diploma.",

    'depInfoDetails3' => "The Professional License \"Computer Systems and Software\" (SIL in French), L3 diploma in the LMD scheme (Bachelor's degree, 180 European credits): these pages present the professional licenses offered in the IT departments of IUT.",

    'depInfoDetails4' => "SIL license holders are specialists who are able to participate in a software development project and ultimately assume full responsibility for it.",




];
