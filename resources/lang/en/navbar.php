<?php

return[

	'home' => "Home",

	'formations' => "Training",

	'formation' => "Education",

	'about' => "About the IUT",

	'IUTInFr' => "French IUT Map",

	'pres' => "Introduction",

	'tem' => "Testimonials",

	'companies' => "Companies" ,

	'after' => "After the IUT",

	'part' => "Partners",

	'offers' => "Internship offers",

	'deb' => "Opportunities",

	'studies' => "Study Pursuit",

	'dut' => "DUT",

	'lp' => "Professional Licenses",

	'news' => "News"



];