<?php

return[

	'title' =>"The first vocation of the IUT is to lead directly to the professional sector, but the students have the possibility of continuing their course if they wish to do so",

	'after' => "Possibilities:",

	'l1'=>"Schools of engineers (ENSIMAG, ISIM, ENSEIHT, IIE, ENSSAT, INT, INSA, IFIPS, UTC, ...)",

	'l2'=>"Degree and Master in computer science",

	'l3'=>"Licence and master in data-processing technology",

	'l4'=>"IUP MIAGE (control of the data-processing methods applied to management)",

	'l5'=>"Further training abroad",

	'l6'=>"Block-release training (preparation with the DEST)",

	'l7'=>"Data-processing Master",

	'l8'=>"Professional degree information processing systems and software",

	'button' => "Learn more about professional licenses"

];