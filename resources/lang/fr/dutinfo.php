<?php

return[
	
    'header' => "Le DUT Informatique",
    
    'intro' => "Introduction",

    'introtext' => "Ce DUT forme des assistants ingénieurs et des chefs de projet en informatique de gestion et en informatique industrielle. Immédiatement opérationnels en développement logiciel et matériel, ils participent à la conception, la réalisation et la mise en œuvre de systèmes informatiques en fonction des cahiers des charges qui leur sont soumis. Ils sont capables de répondre aux besoins des entreprises en matière d'administration de réseaux, de conception et de réalisation de programmes, d'assistance technique, de gestion de bases de données…",

    'introtext2' => "Au fil des années, le titulaire du diplôme peut exercer les fonctions de spécialiste méthodes, d'architecte réseau, de développeur-intégrateur de sites Internet ou de bases de données, de responsable bureautique, de technico-commercial, de spécialiste de systèmes d'imagerie (télédétection, télémédecine, vision par ordinateur…).",

    'jobexample' => "Exemples de métiers:",
    
    'job1' => "analyste-programmeur",

    'job2' => "développeur d'applications mobiles",

    'job3' => "spécialiste bases de données",

    'job4' => "informaticien industrielle",

    'job5' => "intégrateur web",

    'job6' => "technicien de maintenance en informatique",

    'job7' => "testeur en informatique",

    'job8' => "chef de projet",

    'jobothers' => "et bien d'autres ...",

    'access' => "Accès à la formation",

    'accesstext' => "Etre titulaire d’un baccalauréat généraliste (prioritairement scientifique), technologique ou professionnel ou faire une VAE (validation des acquis de l’expérience ou études personnelles). Les titulaires des bacs S et SI sont les plus représentés dans cette formation.",

    'accesstext2' => "L'accès à cette formation se fait sur dossier, voire après avoir passé certains tests et/ou entretien. Il est également possible de rentrer directement en deuxième année (pour faire son DUT en 1 an) si le candidat a validé 60 crédits ECTS ou suivi un enseignement supérieur de 2 ans.",

    'discover' => "Découvrir le contenu de la formation",



];