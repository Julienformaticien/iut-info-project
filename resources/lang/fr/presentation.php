<?php

return[

	'header' => "Présentation de l'IUT Informatique",

    'title' => "L'IUT c'est",
    
    'nbIut' => "IUT en France",

    'nbAns' => "Ans d'existence",

    '3forces' => "La force et le succès de l'IUT en 3 points :",

    '1' => "1",
    '1detail' => "Une formation qui couvre la plupart des secteurs",

    '2' => "2",
    '2detail' => "La collaboration de professionnel(le)s",

    '3' => "3",
    '3detail' => "Des enseignant(e)s chercheurs/euses",

    '6' => "6",
    'mois' => "mois",

    'tpMoy' => "C'est le temps moyen qu'il faut à un(e) étudiant(e) pour trouver un travail après délivrance du diplôme",

    'VAE' => "VAE",

    'VAEdetails' => "Des modes pédagogiques différents sont mis en place, qui conviennent ainsi à un plus grand nombre d'étudiants et de professionnels souhaitant reprendre des études ou faire une validation d'acquis de l'expérience (VAE).",

    'etranger' => "Les IUT, en partenariat avec des universités étrangères, proposent des poursuites d'études à l'étranger, ainsi que des stages.",

    'depInfo' => "Les départements informatiques",

    'depInfoDetails1' => "L'enseignement de l'informatique à l'IUT fait une large part à la dimension professionnelle tout en conservant des composantes fondamentales; l'étudiant acquiert ainsi des savoir-faires mais également des connaissances, des concepts de base et des méthodes de travail qu'il sera à même de mettre à profit en situation professionnelle.",

    'depInfoDetails2' => "Le DUT, diplôme universitaire et professionnel (niveau L2 du schéma LMD, 120 crédits européens): Les titulaires du DUT en informatique sont des techniciens supérieurs, capables de participer à la conception, la réalisation et la mise en oeuvre de systèmes informatiques. Ils peuvent cependant s'ils le désirent poursuivre leurs études à l'issue de ce diplôme.",

    'depInfoDetails3' => "La Licence Professionnelle \"Systèmes Informatiques et Logiciels\" (SIL), diplôme de niveau L3 dans le schéma LMD (grade de Licence, 180 crédits européens): ces pages présentent les licences professionnelles proposées dans les départements informatiques d'IUT.",

    'depInfoDetails4' => "Les titulaires de la licence SIL sont des spécialistes capables de participer à un projet de développement logiciel et, à terme, d'en prendre la complète responsabilité.",




];