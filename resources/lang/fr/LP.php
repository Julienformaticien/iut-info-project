<?php

return[

    'header' => "Les Licences Professionnelles",
    
    'intro' => "Introduction",

    'introText' => "Devant la multiplicité des licences professionnelles informatique, le réseau des 46 départements informatique d'IUT, fort de son expérience depuis 40 ans, a décidé de mettre en avant ses licences professionnelles. Elles ont en commun ce qui a forgé la réputation des DUT informatique : ",

    'force1' => "un enseignement technologique dispensé par des professionnels, des enseignants et des chercheurs du domaine informatique",

    'force2' => "un enseignement pratique avec de nombreux TP et un projet professionnel",

    'force3' => "des matériels et des logiciels performants répondant aux besoins du secteur",

    'force4' => "un encadrement de tous les instants : suivi et contrôle réguliers, travail en petit groupe et en équipe",

    'force5' => "le souci permanent d'être en adéquation avec le tissu économique : formations par alternance, stages de pré-emplois",

    'force6' => "une sélection des étudiants à l'entrée fondée sur leur motivation",

    'force7' => "une expérience concrète en entreprise avec un stage long (4 à 5 mois)",

    'LPSpecialHeader' => "Licences professionnelles de spécialisation en informatique",

    'LPSpecialText' => "Elles consistent à spécialiser des bac + 2 à thématique informatique dans un domaine ou une technologie de pointe (systèmes d'information répartis ou distribués ; administration systèmes, réseau et BD ; développement  internet/intranet ; informatique embarquée ; imagerie et/ou son numérique ...). Elles sont à voir comme une troisième année d'IUT et apportent un haut niveau de technicité. Selon le diplôme d'accès à bac+2, les étudiants auront eu plus de 2100 heures de formation, 2 ou 3 projets tuteurés et 2 stages en entreprises.",

    'LPDiscover' => "Découvrir les Licences Professionnelles",

    'LPDoubleCompHeader' => "Licences professionnelles double compétence",

    'LPDoubleCompText' => "Ces licences visent à apporter une compétence complémentaire à l'informatique ou en informatique. Dans le premier cas, l'enseignement porte à la fois sur l'informatique et sur une autre discipline. Dans le dernier cas, elles posent les bases de l'analyse et de la programmation informatique.",


    'LPspecHeader' => "Les Licences Professionnelles de spécialisation en informatique",

    'GLSI' => "Génie logiciel, systèmes d'information, technologie objet et informatique répartie",

    'MN' => "Métiers du NET",

    'SR' => "Systèmes Réseaux",

    'ISN' => "Image et/ou son numérique",

    'IE' => "Informatique embarquée",

];