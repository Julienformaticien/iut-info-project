<?php
return[
	'title1' => 'BIENVENUE',

	'title2' => 'à l\'IUT Informatique',

	'decouvrir'=> 'Découvrir la formation',

	'typed1' => "Les départements informatique des Instituts Universitaires de Technologies proposent une formation pluri-disciplinaire et polyvalente dans les métiers de l'informatique.",

	'typed2'=> "Elles forment les étudiants à la programmation, l'algorithmique, aux réseaux, systèmes, etc …",

	'why' => 'Pourquoi choisir un I.U.T. ?',

	'article' => 'Découvrir l\'article',

	'moreArticles' => 'Voir plus d\'articles',

	'moreTem' => 'Voir plus de témoignages',

	'readMore' => 'Lire la suite',

	'frenchIUTs'=> 'des iut de France',

	'avForm' => 'et avis sur la formation',

	'test' => 'Témoignages',

	'actu' => 'Actualités',

	'and' =>  '&',

	'news' => 'News',


];