<?php

return[

	'header' => "La formation",

    'intro' => "Introduction",
    
    'nbIut' => "IUT en France",

    'sentence1' => "Les départements d'informatique des IUT de France forment des étudiants à l'informatique et au travail en équipe afin de les insérer dans la vie professionnelle.",

    'sentence2' => "Ils proposent ainsi une formation à taille humaine, encadrée par des professeurs disponibles , ainsi que par des professionnels expérimentés qui apportent aux étudiants une expérience pratique et concrète des métiers de l'informatique.",

    'analyseTitle' => "Analyse </br> &amp; conception",
    'analyseDesc' => "Travail préliminaire en relation avec le client pour spécifier son besoin et ses attentes et la conception du schéma d'un traitement informatique.",

    'algoTitle' => "Algorithmique &amp; programmation",
    'algoDesc' => "Mise en oeuvre effective du traitement jusqu'au codage. Esprit logique, précision technique",

    'archiTitle' => "Architecture système &amp; réseaux",
    'archiDesc' => "Fonctionnement d'un ordinateur, de son système d'exploitation et des réseaux (Internet, Intranet, ...).",

    'ajouts' => "À ces enseignements s'ajoutent",

    'math' => "Mathématiques",
    'eco' => "Économie",
    'gest' => "Gestion",
    'ang' => "Anglais",
    'com' => "Communication",

    'lastText' => "En outre, l'évaluation se fait non seulement sur la théorie mais également sur des mises en situation, via des projets tuteurés et un stage en entreprise, permettant de valider les connaissances et compétences acquises lors de la formation.",


    
    'prog' => "Programme officiel",

    'fr' => "Version française",
    'de' => "Version allemande",
    'es' => "Version espagnole",


];