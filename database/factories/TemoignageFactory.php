<?php

use Faker\Generator as Faker;
use App\Temoignage;

$factory->define(Temoignage::class, function (Faker $faker) {
    return [
        'title' => $faker->text($maxNbChars = 50),
        'author' => $faker->name(),
        'author_info' => $faker->text($maxNbChars = 20), 
        'description' => $faker->text($maxNbChars = 1000)  ,
        // 'image' => $faker->image('app/public/storage/image' ,400,300, 'cat', false)
    ];
});
