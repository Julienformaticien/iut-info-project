<?php

use Faker\Generator as Faker;
use App\Article;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text($maxNbChars = 50),
        'author' => $faker->name(),
        // 'photo' => $faker->image('app/storage/public/image',400,300, null, false),
        'description' => $faker->paragraph($nbSentences = 30 , $variableNbSentences = true),
    ];
});
