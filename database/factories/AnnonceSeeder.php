<?php

use Faker\Generator as Faker;
use App\Announce;

$factory->define(Announce::class, function (Faker $faker) {
    return [
        'title' => $faker->text($maxNbChars = 50),
        'type' => $faker->randomElement(["stage" ,"apprentissage"]),
        'date_beginning' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'minimum_duration' => $faker->randomElement(["1 mois" ,"2 mois","3 mois"]),
        'company_name' => $faker->company(),
        'company_country' => $faker->country(),
        'company_city' => $faker->city(),
        'company_address' => $faker->streetAddress (),
        'company_zipcode' => $faker->postcode(),
        'contact_firstname' => $faker->firstName(),
        'contact_lastname' => $faker->lastName(),
        'contact_phone' => $faker->e164PhoneNumber(),
        'contact_email' => $faker->email(),
        'description' => $faker->text($maxNbChars = 1000),
        'validated' => '1',
        'visible' => '1'
    ];
});
